For system administrators wanting a multi-install on multiple machines. You will need an institutional forum licence.
by T. Carpentier


HOW TO:

BEFORE Copying the System (or cloning) to other machines, please follow these instructions:

First Step :

it is important NOT to authorize them priorly on your master system. You can install the Softwares, but DO NOT Launch them on your « master clone ».

In order to be completely sure that these are not authorized, you should check the existence of a hidden file starting by ._ifpk either in your administrator $HOME, or in this location :

/Library/Application\ Support/Ircam

If either exist, please do remove them BEFORE cloning your system or copying them to other machines.

> rm -f ~/._ifpk_*
> rm -f /Application Support/Ircam/._ifpk_*

Second Step :

– Download this script : ifpCheckOrUpdate

– Make it an executable
> chmod 755 ifpCheckOrUpdate

* Idealy you want to install it along other Unix binaries such as in /usr/local/bin/
* IMPORTANT: The script must be installed on all machines.

– Create the “Ircam” folder in /Library/Application\ Support/ folder
> mkdir -p -m0755 /Library/Application\ Support/Ircam

Last Step :

Run the script on all machines remotely, via ssh or you can use Apple Remote Desktop. Example :

/usr/local/bin/ifpCheckOrUpdate -p "SpatMaxMsp" -d "30/09/2010" -k "the_key_from_forumnet"

Here is the man page of the command :

ifpCheckOrUpdate is a Mac binary that allows you to enter the activation key through command line specially made for network installations.

Example :

./ifpCheckOrUpdate -p "SpatMaxMsp" -d "30/09/2010" -k "the_key_from_forumnet"

“SpatMaxMsp” : input any ircam forum software.

“30/09/2010″ : The date is not important; But it should be prior to the key generation date.
